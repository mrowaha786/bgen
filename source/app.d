import std.stdio;

import utils.misc;

import arsd.png;

import std.conv : to;

/*
 * Args format:
 * bgen <width>x<height> <block_width> <color0> <color1> <color2> <...> output_file_name
 */

struct RGB{
	ubyte r, g, b;

	this(ubyte red, ubyte green, ubyte blue){
		r = red;
		g = green;
		b = blue;
	}
}

void main(string[] args){
	import std.stdio;
	if (args.length < 4){
		writeln("bgen version 0.1\n",
			"a program to generate random images with blocks");
		writeln("Not enough arguments");
		writeln("Arguments format:");
		writeln("bgen [blocks_horizontal]x[block_vertical] [block_width] [color0 in hex] [color1 in hex] [colors...] [output_file_name]");
	}else{
		uinteger height, width, blockSize;
		{
			uinteger[2] size = readSize(args[1]);
			width = size[0];
			height = size[1];
			blockSize = to!uinteger(args[2]);
		}
		// read the colors
		RGB[] colors;
		{
			string[] colorStrings = args[3 .. args.length - 1];
			colors.length = colorStrings.length;
			for (uinteger i = 0; i < colorStrings.length; i ++){
				colors[i] = hexToColor(colorStrings[i]);
			}
		}
		// first tell user that I'm starting
		{
			writeln("bgen starting - image information:");
			writeln("blocks-width: ", width, "\t actual-width (pixels): ",width*blockSize);
			writeln("blocks-height: ", height, "\t actual-height (pixels): ",height*blockSize);
			writeln("total colors: ", colors.length);
		}
		
		// now make an image
		TrueColorImage image = new TrueColorImage(cast(int)(width*blockSize), cast(int)(height*blockSize));
		auto colorData = image.imageData.colors;
		uinteger actualWidth = width*blockSize, actualHeight = height*blockSize;
		Color[] rowColors;
		for (uinteger row = 0; row < actualHeight; row ++){
			if (row%blockSize == 0){
				rowColors = generateRow(colors, width);
			}
			// write this row
			for (uinteger col = 0; col < actualWidth; col ++){
				colorData[(row * actualWidth) + col] = rowColors[col/blockSize];
			}
		}
		// write image to file
		writePng(args[args.length-1], image);
		{
			writeln("bgen finished - image written to: \n\t", args[args.length-1]);
		}
	}

}

/// reads size from a <width>x<height> format, returns index0=width; index1=height
uinteger[2] readSize(string s){
	uinteger[2] size;
	for (uinteger i = 0; i < s.length; i ++){
		if (s[i] == 'x'){
			// read previous
			string width = s[0 .. i], height = s[i+1 .. s.length];
			// check if is valid
			if (width.length == 0 || height.length == 0 || !isNum(width) || !isNum(height)){
				throw new Exception("not a valid width/height");
			}
			size[0] = to!uinteger(width);
			size[1] = to!uinteger(height);
		}
	}
	return size;
}

/// generates a random row
Color[] generateRow(RGB[] colors, uinteger length){
	import std.random;
	Color[] r;
	r.length = length;
	for (uinteger i = 0; i < length; i ++){
		RGB currentColor = colors[uniform(0, colors.length)];
		r[i] = Color(currentColor.r, currentColor.g, currentColor.b);
	}
	return r;
}

///Converts hex color code to RGB
RGB hexToColor(string hex){
	import utils.baseconv;
	RGB r;
	uinteger den = hexToDenary(hex);
	//min val for red in denary = 65536
	//min val for green in denary = 256
	//the remaining value is blue
	if (den >= 65536){
		r.r = cast(ubyte)((den / 65536));
		den -= r.r*65536;
	}
	if (den >= 256){
		r.g = cast(ubyte)((den / 256));
		den -= r.g*256;
	}
	r.b = cast(ubyte)den;
	return r;
}
///
unittest{
	RGB c;
	c.r = 10;
	c.g = 15;
	c.b = 0;
	assert("0A0F00".hexToColor == c);
}

///Converts RGB to hex color code
string colorToHex(RGB col){
	import utils.baseconv;
	uinteger den;
	den = col.b;
	den += col.g*256;
	den += col.r*65536;
	return denaryToHex(den);
}
///
unittest{
	RGB c;
	c.r = 10;
	c.g = 8;
	c.b = 12;
	assert(c.colorToHex == "A080C");
}
